<?php

declare(strict_types=1);

namespace CoStack\BuildEssentials\Composer;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Package\PackageInterface;
use Composer\Plugin\PluginInterface;
use Composer\Util\Filesystem;
use CoStack\BuildEssentials\ContainerCompiler;
use RuntimeException;
use Throwable;

use function CoStack\Lib\concat_paths;
use function getcwd;
use function key;
use function realpath;
use function reset;
use function rtrim;
use function str_starts_with;

class Plugin implements PluginInterface, EventSubscriberInterface
{
    protected Composer $composer;
    protected IOInterface $io;

    public function activate(Composer $composer, IOInterface $io)
    {
        $this->composer = $composer;
        $this->io = $io;
    }

    public function deactivate(Composer $composer, IOInterface $io)
    {
        $this->composer = $composer;
        $this->io = $io;
    }

    public function uninstall(Composer $composer, IOInterface $io)
    {
        $this->composer = $composer;
        $this->io = $io;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'post-autoload-dump' => 'execute',
        ];
    }

    public function execute(): void
    {
        $config = $this->composer->getConfig();
        $filesystem = new Filesystem();
        $vendorPath = $filesystem->normalizePath(realpath(realpath($config->get('vendor-dir'))));
        require concat_paths($vendorPath, 'autoload.php');

        $rootPackage = $this->composer->getPackage();
        if ($this->shouldBuildForPackage($rootPackage)) {
            $installPath = getcwd();
            $this->buildForPackage($rootPackage, $installPath);
        }

        $packages = $this->composer->getRepositoryManager()->getLocalRepository()->getPackages();
        foreach ($packages as $package) {
            if ($this->shouldBuildForPackage($package)) {
                $installPath = $this->composer->getInstallationManager()->getInstallPath($package);
                $installPath = rtrim($installPath, '/') . '/';
                $this->buildForPackage($package, $installPath);
            }
        }
    }

    protected function shouldBuildForPackage(PackageInterface $package): bool
    {
        if (empty($package->getAutoload()['psr-4'])) {
            return false;
        }
        foreach ($package->getRequires() as $requireLink) {
            $target = $requireLink->getTarget();
            if ('co-stack/build-essentials' === $target) {
                return true;
            }
        }
        return false;
    }

    protected function buildForPackage(PackageInterface $package, string $installPath): void
    {
        $autoload = $package->getAutoload()['psr-4'];
        $namespaceRoot = reset($autoload);
        $namespace = key($autoload);

        $extra = $package->getExtra()['co-stack/build-essentials'] ?? [];
        $resource = $extra['resource'] ?? $installPath;
        $namespace = $extra['namespace'] ?? ($namespace . 'Generated');
        $folderPath = $extra['folder'] ?? concat_paths($namespaceRoot, 'Generated');
        if (!str_starts_with($resource, '/')) {
            $resource = concat_paths($installPath, $resource);
        }
        if (!str_starts_with($folderPath, '/')) {
            $folderPath = concat_paths($installPath, $folderPath);
        }

        try {
            $containerCompiler = new ContainerCompiler();
            $containerCompiler->build(
                $resource,
                $namespace,
                $folderPath,
            );
        } catch (Throwable $exception) {
            throw new RuntimeException(
                'Error compiling the container for package ' . $package->getName() . ': '
                . $exception->getMessage(),
                1701810741, $exception,
            );
        }
    }
}
