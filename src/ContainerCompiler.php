<?php

declare(strict_types=1);

namespace CoStack\BuildEssentials;

use Symfony\Component\Config\ConfigCache;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Loader\DelegatingLoader;
use Symfony\Component\Config\Loader\LoaderResolver;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Dumper\PhpDumper;
use Symfony\Component\DependencyInjection\Loader\DirectoryLoader;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

use function CoStack\Lib\concat_paths;
use function CoStack\Lib\mkdir_deep;
use function trim;

class ContainerCompiler
{
    public function build(string $resource, string $namespace, string $folderName): string
    {
        $container = new ContainerBuilder();
        $loaderResolver = new LoaderResolver([
            new YamlFileLoader($container, new FileLocator()),
            new PhpFileLoader($container, new FileLocator()),
            new DirectoryLoader($container, new FileLocator()),
        ]);
        $loader = new DelegatingLoader($loaderResolver);
        $loader->import($resource);
        $container->compile();
        $dumper = new PhpDumper($container);
        $content = $dumper->dump([
            'class' => 'DI',
            'namespace' => trim($namespace, '\\'),
            'debug' => false,
        ]);
        mkdir_deep($folderName);
        $cache = new ConfigCache(concat_paths($folderName, '/DI.php'), false);
        $cache->write($content);
        return $cache->getPath();
    }
}
