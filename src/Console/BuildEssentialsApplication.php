<?php

declare(strict_types=1);

namespace CoStack\BuildEssentials\Console;

use CoStack\BuildEssentials\Console\Command\CompileDependencyInjectionContainer;
use CoStack\BuildEssentials\ContainerCompiler;
use Symfony\Component\Console\Application;

class BuildEssentialsApplication extends Application
{
    public function __construct(string $name = 'co-stack/build-essentials', string $version = '0.1-alpha')
    {
        parent::__construct($name, $version);
        $this->add(new CompileDependencyInjectionContainer(new ContainerCompiler()));
    }
}
