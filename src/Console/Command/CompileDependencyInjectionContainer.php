<?php

declare(strict_types=1);

namespace CoStack\BuildEssentials\Console\Command;

use CoStack\BuildEssentials\ContainerCompiler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CompileDependencyInjectionContainer extends Command
{
    public function __construct(protected readonly ContainerCompiler $containerCompiler)
    {
        parent::__construct('di:compile');
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                'resource',
                InputArgument::REQUIRED,
                'Your service configuration file or folder (must end with a slash "/")',
            )
            ->addArgument(
                'namespace',
                InputArgument::REQUIRED,
                'Namespace of the generated DI class. Should reflect the psr-4 NS of the folderName.',
            )
            ->addArgument(
                'folderName',
                InputArgument::REQUIRED,
                'Where the DI.php file will be written to',
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $resource = $input->getArgument('resource');
        $namespace = $input->getArgument('namespace');
        $folderName = $input->getArgument('folderName');

        $this->containerCompiler->build($resource, $namespace, $folderName);

        return self::SUCCESS;
    }
}
